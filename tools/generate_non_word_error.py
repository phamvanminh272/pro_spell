import random
import editdistance
with open('./data/common-vietnamese-syllables.txt', 'r') as f:
    dict_0 = f.read()
LEGAL_C = "abcdefghijklmnopqrstuvwxyzáàảãạăắằẳẵặâấầẩẫậéèẻẽẹêếềểễệíìỉĩịóòỏõọôốồổỗộơớờởỡợúùủũụưứừửữựýỳỷỹỵđ"
def generate_non_word_error(word, distance=1, dict=None):
    if dict == None:
        dict = dict_0
    non_word_error = word
    while non_word_error in dict:
        ls_c = [c for c in word]
        random_character = LEGAL_C[random.randint(0, len(LEGAL_C) - 1)]
        ls_c[random.randint(0, len(word) - 1)] = random_character
        non_word_error = ''.join(ls_c)

    if distance == 2:
        while editdistance.eval(non_word_error, word)!=2:
            ls_c = [c for c in non_word_error]
            random_character = LEGAL_C[random.randint(0, len(LEGAL_C) - 1)]
            ls_c[random.randint(0, len(word) - 1)] = random_character
            non_word_error = ''.join(ls_c)
    return non_word_error