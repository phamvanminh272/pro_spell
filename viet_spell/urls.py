from django.urls import path
from . import views
from .views import TextCreateView, TextListView

urlpatterns = [
    path('', TextCreateView.as_view(), name='viet_spell_home'),
    path('about/', views.about, name='viet_spell_about'),
    path('list/', TextListView.as_view(), name='viet_spell_list'),
]
