from django.db import models
from django.utils import timezone
from datetime import datetime
from django.urls import reverse
class Texts(models.Model):
    text = models.TextField()
    check_non_word_errors = models.BooleanField(default=False)
    add_tone = models.BooleanField(default=False)
    corrected_text = models.TextField(default='')
    non_word_errors = models.TextField(default='')
    number_candidates = models.TextField(default='')
    corrected_words = models.TextField(default='')
    date_posted = models.DateTimeField(default=timezone.now)
    def __str__(self):
        return self.title
    # def get_absolute_url(self):
    #     return reverse('viet_spell_home')
