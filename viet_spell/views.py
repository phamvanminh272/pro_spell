from django.shortcuts import render
from .models import Texts
from django.views.generic import CreateView, ListView
from django.http import HttpResponseRedirect
# from pro_check import predict
from pro_check_v2 import predict, parse_result
from pro_add_tone import predictor_add_tone

def home(request):
    return render(request, 'viet_spell/home.html')

class TextListView(ListView):
    model = Texts
    template_name = 'viet_spell/home.html'
    context_object_name = 'texts'
    ordering = ['-date_posted']

class TextCreateView(CreateView):
    model = Texts
    fields = [ 'check_non_word_errors', 'add_tone', 'text']

    def form_valid(self, form):
        self.object = form.save(commit=False)
        text = form.cleaned_data['text']
        check_non_word_errors = form.cleaned_data['check_non_word_errors']
        add_tone = form.cleaned_data['add_tone']
        if add_tone != False and check_non_word_errors != False:
            corrected_text = predictor_add_tone.predict(text)
            corrected_text, ls_non_word_errors = predict.beam_lm(corrected_text)
            corrected_text, confuse_words = predict.beam_lm2(corrected_text)
            self.object.corrected_text = corrected_text
            # self.object.non_word_errors = ' '.join(ls_non_word_errors)
            self.object.non_word_errors = ''
            self.object.save()
        if add_tone == False and check_non_word_errors == False:
            corrected_text = predictor_add_tone.predict(text)
            corrected_text, ls_non_word_errors = predict.beam_lm(corrected_text)
            corrected_text, confuse_words = predict.beam_lm2(corrected_text)
            self.object.corrected_text = corrected_text
            # self.object.non_word_errors = ' '.join(ls_non_word_errors)
            self.object.non_word_errors = ''
            self.object.save()
        if add_tone != False:
            corrected_text = predictor_add_tone.predict(text)
            self.object.corrected_text = corrected_text
            self.object.save()
        if check_non_word_errors != False:
            corrected_text, ls_non_word_errors = predict.beam_lm(text)
            corrected_text, confuse_words = predict.beam_lm2(corrected_text)
            self.object.corrected_text = corrected_text
            # self.object.non_word_errors = ' '.join(ls_non_word_errors)
            self.object.non_word_errors = ''
            self.object.save()
        return HttpResponseRedirect('/spell/list')

def about(request):
    # predicted_seq = predict.beam_lm(predicted_seq='Minh đi chơii nhé')
    predicted_seq = ''
    context = {
        'post': predicted_seq
    }
    return render(request, 'viet_spell/about.html', context)
