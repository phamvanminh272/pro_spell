from django.apps import AppConfig


class VietSpellConfig(AppConfig):
    name = 'viet_spell'
